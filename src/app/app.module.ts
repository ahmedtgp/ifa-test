import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { programsComponent } from './programs/programs.component';
import { admissionComponent } from './admission/admission.component';
import { sponserComponent } from './sponser/sponser.component';
import { footerComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    programsComponent,
    admissionComponent,
    sponserComponent,
    footerComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  

    
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
